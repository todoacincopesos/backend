<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InsertZipCodesSeed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        $dumpDir = 'database/migrations/dumps/';
//
//        $file = fopen($dumpDir . 'zip_codes.csv', "r");
//        $isFirstRow = true;
//
//        while (($row = fgetcsv($file, ",")) == true) {
//            if (count($row) !== 15) {
//                continue;
//            }
//
//            if ($isFirstRow) {
//                $isFirstRow = false;
//                continue;
//            }
//
//            $this->insertRow($row);
//        }
    }

    private function insertRow($row)
    {
//        DB::table('zip_codes')->insert([
//            [
//                'zip_code'            => $this->getRawValue($row[0]),
//                'settlement'          => $this->getRawValue($row[1]),
//                'settlement_type'     => $this->getRawValue($row[2]),
//                'municipality'        => $this->getRawValue($row[3]),
//                'state'               => $this->getRawValue($row[4]),
//                'city'                => $this->getRawValue($row[5]),
//                'pa_zip_code'         => $this->getRawValue($row[6]),
//                'state_key'           => $this->getRawValue($row[7]),
//                'office'              => $this->getRawValue($row[8]),
//                'cp'                  => $this->getRawValue($row[9]),
//                'settlement_type_key' => $this->getRawValue($row[10]),
//                'municipality_key'    => $this->getRawValue($row[11]),
//                'settlement_id'       => $this->getRawValue($row[12]),
//                'zone'                => $this->getRawValue($row[13]),
//                'city_key'            => $this->getRawValue($row[14])
//            ]
//        ]);
    }

//    private function getRawValue($value) {
//        if (empty($value)) {
//            return null;
//        }
//
//        return $value;
//    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        DB::unprepared('TRUNCATE zip_codes;');
    }

}
