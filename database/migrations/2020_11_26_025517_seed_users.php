<?php

use App\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class SeedUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $userAdmin = User::create([
                'name' => 'admin',
                'lastname' => 'admin',
                'gender' => 'M',
                'email' => 'admin@admin.com',
                'password' => Hash::make('123456789'),
                'phone' => '3322545833',
                'zip_code' => '45140',
                'settlement' => 'Santa Margarita',
                'municipality' => 'Zapopan',
                'state' => 'Jalisco',
            ]);
        $userAdmin->assignRole('Admin');
        $userEmployee = User::create([
            'name' => 'employee',
            'lastname' => 'employee',
            'gender' => 'M',
            'email' => 'employee@employee.com',
            'password' => Hash::make('123456789'),
            'phone' => '3345545831',
            'zip_code' => '45140',
            'settlement' => 'El Vigia',
            'municipality' => 'Zapopan',
            'state' => 'Jalisco',
        ]);
        $userEmployee->assignRole('Employee');
        $userCustomer = User::create([
            'name' => 'customer',
            'lastname' => 'customer',
            'gender' => 'M',
            'email' => 'customer@customer.com',
            'password' => Hash::make('123456789'),
            'phone' => '3322573433',
            'zip_code' => '45140',
            'settlement' => 'La Loma',
            'municipality' => 'Zapopan',
            'state' => 'Jalisco',
        ]);
        $userCustomer->assignRole('Customer');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $userAdmin = User::findByName('admin'); $userAdmin->delete();
        $userEmployee = User::findByName('employee'); $userEmployee->delete();
        $userCustomer = User::findByName('customer'); $userCustomer->delete();
    }
}
