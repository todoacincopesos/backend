<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableOrdersAddFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('credit_card_number')->nullable();
            $table->string('credit_card_name')->nullable();
            $table->integer('credit_card_cvc')->nullable();
            $table->integer('expiration_month')->nullable();
            $table->integer('expiration_year')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('expiration_year');
            $table->dropColumn('expiration_month');
            $table->dropColumn('credit_card_cvc');
            $table->dropColumn('credit_card_name');
            $table->dropColumn('credit_card_number');
        });
    }
}
