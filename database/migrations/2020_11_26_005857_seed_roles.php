<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class SeedRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $roleAdmin = Role::create(['name' => 'Admin']);
        $roleEmployee = Role::create(['name' => 'Employee']);
        $roleCustomer = Role::create(['name' => 'Customer']);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $role = Role::findByName('Admin'); $role->delete();
        $role = Role::findByName('Employee'); $role->delete();
        $role = Role::findByName('Customer'); $role->delete();
    }
}
