<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category()
    {
        return $this->hasOne(Category::Class);
    }

    public function cart()
    {
        return $this->belongsTo(Cart::Class);
    }

    public function orderDetail()
    {
        return $this->belongsTo(OrderDetail::Class);
    }
}
