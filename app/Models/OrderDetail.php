<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    public function product()
    {
        return $this->hasOne(Product::Class);
    }

    public function order()
    {
        return $this->belongsTo(Order::Class);
    }
}
