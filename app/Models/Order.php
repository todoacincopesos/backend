<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    public function orderDetails()
    {
        return $this->hasMany(OrderDetail::Class);
    }

    public function user()
    {
        return $this->belongsTo(User::Class);
    }
}
