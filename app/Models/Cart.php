<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

    public function product()
    {
        return $this->hasOne(Product::Class);
    }

    public function user()
    {
        return $this->belongsTo(User::Class);
    }
}
