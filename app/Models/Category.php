<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{

    protected $guarded = [];

    public function product()
    {
        return $this->belongsTo(Product::Class);
    }
}
