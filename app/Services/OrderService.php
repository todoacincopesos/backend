<?php


namespace App\Services;

use App\Models\Cart;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\ZipCode;


class OrderService{

    public static function addOrderDetails($orderId, $user_id){
        $cart = Cart::where('user_id', $user_id)->get();
        $total_price_order = 0;

        foreach ($cart as $productInCart) {

            $product = Product::where('id', $productInCart['product_id'])->first();
            if ($product == null) {
                return response()->json(['message' => "No existe producto"], 400);
            }

            if ($product->stock == 0 || $productInCart['quantity'] > $product->stock) {
                return response()->json(['message' => "No hay stock suficiente"], 400);
            }

            $orderDetail = new OrderDetail();
            $orderDetail->order_id = $orderId;
            $orderDetail->product_id = $product->id;
            $orderDetail->description = $product->description;
            $orderDetail->quantity = $productInCart['quantity'];
            $orderDetail->total_price = $productInCart['total_price'];
            $orderDetail->save();

            $total_price_order = $orderDetail->total_price + $total_price_order;

            $product->stock = $product->stock - $productInCart['quantity'];
            $product->update();

            $cartElement = Cart::find($productInCart['id']);
            $cartElement->delete();
        }

        return $total_price_order;
    }

    public static function getProductsFromOrder($orderDetails){


        $products = [];
        foreach ($orderDetails as $orderDetail){
            $product = Product::find($orderDetail['product_id']);
            $products[] = [
                'id' => $orderDetail->id,
                'product_description' => $product->description,
                'product_name' => $product->unit_price,
                'product_image' => $product->image,
                'quantity' => $orderDetail->quantity,
                'total_price' => $orderDetail->total_price,
            ];
        }

        return $products;
    }

}

