<?php


namespace App\Services;

use App\Models\ZipCode;


class ZipCodeService{

    public static function getSettlementsAll($zipcode){
        $zipcodes = ZipCode::where('zip_code', $zipcode)->get();
        $settlements = [];

        foreach($zipcodes as $zipcode){
            $settlements[] = $zipcode->settlement;
        }

        return $settlements;
    }

}

