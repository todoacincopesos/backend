<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class CartController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $data = DB::table('carts')
            ->join('products', 'products.id', '=', 'carts.product_id')
            ->select('carts.*', 'products.name', 'products.unit_price', 'products.image')
            ->where('carts.user_id',$user->id)
            ->get();

        $cart = [];

        foreach ($data as $productInCar) {

            $cart[] = [
                'id' => $productInCar->id,
                'product_name' => $productInCar->name,
                'product_unit_price' => $productInCar->unit_price,
                'product_image' => $productInCar->image,
                'quantity' => $productInCar->quantity,
                'total_price' => $productInCar->total_price,
            ];
        }


        return response()->json($cart, $cart == [] ? 204 : 200);
    }

    public function show($id)
    {
        $user = auth()->user();
        $data = DB::table('carts')
            ->join('products', 'products.id', '=', 'carts.product_id')
            ->select('carts.*', 'products.name', 'products.unit_price', 'products.image')
            ->where('carts.user_id',$user->id)
            ->where('carts.id',$id)->first();

        $cart = [
            'id' => $data->id,
            'product_name' => $data->name,
            'product_unit_price' => $data->unit_price,
            'product_image' => $data->image,
            'quantity' => $data->quantity,
            'total_price' => $data->total_price,
        ];

        return $cart != null ? response()->json($cart, 200) : response()->json('No existe Carrito', 400);

    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'product_id' => 'required',
                'quantity' => 'required',
            ]);

        if ($validator->fails()) {
            return response()->json(['message'=>"Error al realizar la accion"], 400);
        }
        $user = auth()->user();

        $product = Product::where('id',$request->product_id)->first();
        if ($product == null) {
            return response()->json(['message'=>"No hay stock suficiente"], 400);
        }

        if($product->stock == 0)
        {
            return response()->json(['message'=>"No hay stock suficiente"], 400);
        }

        $cart = new Cart();
        $cart->user_id = $user->id;
        $cart->product_id = $product->id;
        $cart->quantity = $product->stock < $request->quantity ? $product->stock : $request->quantity;
        $cart->total_price = $product->unit_price * $cart->quantity;
        $cart->save();

        return response()->json([
            "success" => true,
            "message" => "Accion realizada con exito",
        ],200);



    }

    public function update(Request $request, $id)
    {
        $cart = Cart::where('id',$id)->first();
        $product = Product::where('id',$cart->product_id)->first();
        $cart->quantity = $product->stock < $request->quantity ? $product->stock : $request->quantity;
        $cart->total_price = $product->unit_price * $cart->quantity;

        $cart->update();

        return response()->json([
            "success" => true,
            "message" => "Accion realizada con exito",
        ],200);
    }

    public function delete($id)
    {
        $product = Product::where('id',$id)->first();
        $product->delete();

        return response()->json('Accion realizada con exito', 200);
    }
}
