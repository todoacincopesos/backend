<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;


class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        foreach ($products as $product) {
            $category = Category::where('id',$product['category_id'])->first();
            $response[] = [
                'id' => $product['id'],
                'category_id' => $product['category_id'],
                'name' => $product['name'],
                'description' => $product['description'],
                'image' => $product['image'],
                'unit_price' => $product['unit_price'],
                'stock' => $product['stock'],
                'category' => $category->name
            ];
        }

        return response()->json($response, $products == [] ? 204 : 200);
    }

    public function show($id)
    {
        $product = Product::where('id',$id)->first();
        $category = Category::where('id',$product['category_id'])->first();
        $response = [
            'id' => $product['id'],
            'category_id' => $product['category_id'],
            'name' => $product['name'],
            'description' => $product['description'],
            'image' => $product['image'],
            'unit_price' => $product['unit_price'],
            'stock' => $product['stock'],
            'category' => $category->name
        ];
        return $product != null ? response()->json($response, 200) : response()->json('No existe categoria', 401);

    }

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(),
            [
                'category_id' => 'required',
                'name' => 'required',
                'unit_price' => 'required',
                'image' => 'required',
            ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        if ($files = $request->file('image')) {

            //store file into document folder
            $file = $request->file('image')->store('public/products');
            $product = new Product();
            $product->category_id = $request->category_id;
            $product->name = $request->name;
            $product->description = $request->description;
            $product->unit_price = $request->unit_price;
            $product->stock = $request->stock ?? 0;
            $product->image = 'storage/app/'.$file;
            $product->save();

            return response()->json([
                "success" => true,
                "message" => "Accion realizada con exito",
            ],200);

        }

    }

    public function update(Request $request, $id)
    {
        $product = Product::where('id',$id)->first();

        $product->category_id = $request->category_id != null ? $request->category_id : $product->category_id;
        $product->name = $request->name != null ? $request->name : $product->name;
        $product->description = $request->description != null ? $request->description : $product->description;
        $product->unit_price = $request->unit_price != null ? $request->unit_price : $product->unit_price;
        $product->stock = $request->stock != null ? $request->stock : $product->stock;
        if ($request->hasFile('image')) {
            $file = $request->file('image')->store('public/products');
            $product->image = 'storage/app/' . $file;
        }
        $product->update();

        return response()->json([
            "success" => true,
            "message" => "Accion realizada con exito",
        ],200);
    }

    public function delete($id)
    {
        $product = Product::where('id',$id)->first();
        $product->delete();

        return response()->json('Accion realizada con exito', 200);
    }
}
