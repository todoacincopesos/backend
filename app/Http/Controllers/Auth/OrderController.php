<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Services\OrderService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;


class OrderController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $orders = Order::where('user_id',$user->id)->get();
        foreach ($orders as $order) {
            $orderDetails = OrderDetail::where('order_id',$order->id)->get();
            $order->orderDetail = $orderDetails;
        }

        $response = [];

        foreach ($orders as $order) {
            $response[] = [
                'id' => $order->id,
                'date' => $order->date,
                'total_price' => $order->total_price,
                'credit_card_name' => $order->credit_card_name,
                'credit_card_number' => '*************'.Str::substr($order->credit_card_number, 13),
                'order_details' => OrderService::getProductsFromOrder($order->orderDetail)
            ];
        }

        return response()->json([
            'success' => true,
            'message' => "Accion realizada con exito",
            'content' => $response
        ],$response == [] ? 204 : 200);
    }

    public function show($id)
    {
        $user = auth()->user();
        $order = Order::where('user_id',$user->id)->first();

        $orderDetails = OrderDetail::where('order_id',$order->id)->get();
        $order->orderDetail = $orderDetails;


        $response = [
            'id' => $order->id,
            'date' => $order->date,
            'total_price' => $order->total_price,
            'credit_card_name' => $order->credit_card_name,
            'credit_card_number' => '*************'.Str::substr($order->credit_card_number, 13),
            'order_details' => OrderService::getProductsFromOrder($order->orderDetail)
        ];

        return response()->json([
            'success' => true,
            'message' => "Accion realizada con exito",
            'content' => $response
        ],$response == [] ? 204 : 200);
    }

    public function store(Request $request)
    {
        $total_price_order = 0;

        $user = auth()->user();

        $order = new Order();
        $order->user_id = $user->id;
        $order->credit_card_number = $request->credit_card_number;
        $order->credit_card_name = $request->credit_card_name;
        $order->credit_card_cvc = $request->credit_card_cvc;
        $order->expiration_month = $request->expiration_month;
        $order->expiration_year = $request->expiration_year;
        $order->date = now();
        $order->total_price = $total_price_order;
        $order->save();
        $orderId = $order->id;

        $total_price_order = OrderService::addOrderDetails($orderId,$user->id);

        $order->total_price = $total_price_order;
        $order->update();


        return response()->json([
            "success" => true,
            "message" => "Accion realizada con exito",
        ],200);

    }

    public function update(Request $request, $id)
    {
        $order = Order::where('id',$id)->first();

        $order->credit_card_number = $request->credit_card_number != null ? $request->credit_card_number : $order->credit_card_number;
        $order->credit_card_name = $request->credit_card_name != null ? $request->credit_card_name : $order->credit_card_name;
        $order->credit_card_cvc = $request->credit_card_cvc != null ? $request->credit_card_cvc : $order->credit_card_cvc;
        $order->expiration_month = $request->expiration_month != null ? $request->expiration_month : $order->expiration_month;
        $order->expiration_year = $request->expiration_year != null ? $request->expiration_year : $order->expiration_year;

        $order->update();

        return response()->json([
            "success" => true,
            "message" => "Accion realizada con exito",
        ],200);
    }

    public function delete($id)
    {
        $order = Order::where('id',$id)->first();
        $orderDetails = OrderDetail::where('order_id',$order->id)->get();
        foreach ($orderDetails as $orderDetail) {
            $data = OrderDetail::find($order->id);
            $data->delete();
        }
        $order->delete();

        return response()->json('Accion realizada con exito', 200);
    }
}
