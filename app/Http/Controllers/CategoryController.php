<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return response()->json($categories, $categories == [] ? 204 : 200);
    }

    public function show($id)
    {
        $category = Category::where('id',$id)->first();
        return $category != null ? response()->json($category, 200) : response()->json('No existe categoria', 401);
    }

    public function store(Request $request)
    {
        $category = Category::create($request->all());

        return response()->json($category, 201);
    }

    public function update(Request $request, $id)
    {
        $category = Category::where('id',$id)->first();
        $category->update($request->all());


        return response()->json($category, 200);
    }

    public function delete($id)
    {
        $category = Category::where('id',$id)->first();
        $category->delete();

        return response()->json('Accion realizada con exito', 200);
    }
}
