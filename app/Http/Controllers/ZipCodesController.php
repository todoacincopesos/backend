<?php

namespace App\Http\Controllers;

use App\Models\ZipCode;
use Illuminate\Http\Request;
use App\Services\ZipCodeService;


class ZipCodesController extends Controller
{
    public function zipCodeInfo($zipCode)
    {
        $info = ZipCode::where('zip_code', $zipCode)->first();

        if (!$info) {
            return response()->json([
                'municipality' => '',
                'state'        => '',
                'settlements'  => []
            ]);
        }

        return response()->json([
            'municipality' => $info->municipality,
            'state'        => $info->state,
            'settlements'  => ZipCodeService::getSettlementsAll($zipCode)
        ]);
    }
}

