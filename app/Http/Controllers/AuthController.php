<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = $request->user();
            $data['token'] = $user->createToken('MyApp')->accessToken;
            $data['name']  = $user->name;
            $data['role']  = $user->roles->first()->name;
            return response()->json($data, 200);
        }

        return response()->json(['error'=>'Unauthorized'], 401);
    }

    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'lastname' => 'required',
            'gender' => 'required|in:M,F',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $user = $request->all();
        $user['password'] = Hash::make($user['password']);
        $user = User::create($user);
        $user->assignRole('Customer');
        $success['token'] =  $user->createToken('TodoACincoPesos')-> accessToken;
        $success['name'] =  $user->name;


        return response()->json(['success'=>$success],200);
    }

    public function userDetail()
    {
        $user = Auth::user();
        $role = $user->getRoleNames();
        return response()->json(['user' => $user],200);
    }
}
