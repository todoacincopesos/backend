<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesController extends Controller
{
    public function update(Request $request)
    {
        $role = Role::create(['name' => $request->role]);

        return response()->json('Accion realizada con exito', 200);
    }

}
